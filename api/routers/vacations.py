#fastAPI endpoints
from fastapi import APIRouter
from queries.vacations import VacationIn #defined in queries vacations.py file


router = APIRouter()


@router.post("/vacatons")
def create_vacation(vacation: VacationIn): #<= "vacaction: VacationIn" typehints
    print('vacation', vacation)
    return vacation
