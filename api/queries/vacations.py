from typing import Optional
from pydantic import BaseModel # !!! has NOTHING TO DO WITH THE DATABASE !!!


class VacationIn(BaseModel):
    name: str # <= typehints
    from_date: str
    to_date: str
    thoughts: Optional[str]
