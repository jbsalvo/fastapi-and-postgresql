from fastapi import FastAPI
from routers import vacations #imports


app = FastAPI()
app.include_router(vacations.router) #includes endpoints from router
